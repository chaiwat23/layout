package com.example.myprofine;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {
    Button p1,p2;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        p1=(Button)findViewById(R.id.btn2);
        p1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent p1 = new Intent(MainActivity2.this,MainActivity.class);
                startActivity(p1);
            }
        });
        p2=(Button)findViewById(R.id.btn3);
        p2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent p2 = new Intent(MainActivity2.this,MainActivity3.class);
                startActivity(p2);
            }
        });
        }
    }
