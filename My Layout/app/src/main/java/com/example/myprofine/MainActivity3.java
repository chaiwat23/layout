package com.example.myprofine;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity3 extends AppCompatActivity {
    Button p3;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        p3=(Button)findViewById(R.id.btn2);
        Button btn_fb = findViewById(R.id.btn_fb);
        p3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent p3 = new Intent(MainActivity3.this,MainActivity.class);
                startActivity(p3);
            }
        });
        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent btn_fb = new Intent(Intent.ACTION_VIEW);
                btn_fb.setData(Uri.parse("https://www.facebook.com/profile.php?id=100005000886396"));
                startActivity(Intent.createChooser(btn_fb, "open with"));
            }
        });
    }
}